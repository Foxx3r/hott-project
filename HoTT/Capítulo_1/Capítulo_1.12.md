o capítulo 1.12, embora seja muito pobre em termos de conhecimento agregado, introduz uma noção interessante: a diferença entre igualdade de juízos (≡) e igualdade proposicional (ou tipo de identidade; =), onde o último tem um único elemento chamado refl (já falei isso aqui antes)

aliás, vou falar isso antes que seja tarde demais, que não falei até agora por eu pessoalmente não gostar da notação e achar desnecessário, mas o livro HoTT evita ao máximo abuso de notação (pesquise para saber o que é), por isso é escrito coisas como $`x =_A y`$, pois a igualdade se refere a tipos em A, i.e, está contextualizando (eles mesmos já alertaram a importância dessa notação quando o contexto não puder ser inferido)

por ex, em álgebra abstrata, se há um homomorfismo entre, por exemplo, dois grupos A e B, e temos a função de mapeamento que é representado por $`f : A \to B`$, então com abuso de notação, $`f(x + y) = f(x) + f(y), x, y \in A`$, mas o correto formalmente falando, seria contextualizar que nem faz a turma do HoTT, porque na equação do lado direito o + já não pertence mais ao grupo A, mas ao grupo B

ou como a galera de HoTT adoraria escrever: $`f(x +_A y) = f(x) +_B f(y)`$

e enfim, é isso. O último capítulo apenas define desigualdade. Acabamos o capítulo 1
