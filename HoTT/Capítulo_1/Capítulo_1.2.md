o capítulo 1.2 começa falando o que significa A → B, isso a gente já sabe (remark: eu traduziria map como morfismo), mas o interessante é quando ele fala que em teoria dos conjuntos isso é uma relação funcional, mas em tipos é um conceito primitivo

>obs: o primeiro capítulo é somente sobre teoria dos tipos, e não HoTT<

aqui temos a definição de função constante

[imagem da página 22]

no caso, $`(x \mapsto x+1)`$ é apenas uma outra maneira de escrever $`\lambda{x}.x+1`$

também temos que $`g(x,-)`$ é uma abreviação de $`\lambda{y}.g(x, y)`$, onde x que está explícito é uma variável livre

também é usada essa notação para se referir a redução-beta

[idem]

também temos que $`f(x) :\equiv x`$ é igual a $`f :\equiv \lambda{x}.x`$

até aqui, nada demais, se você aprendeu lambda em programação, isso vai soar mais do que natural

depois fala de currying e de redução-alfa, assunto que já vimos, então pularei
