então, o universo é o tipo dos tipos. Todo tipo pertence a um universo. Há universos infinitos, i.e, $`\mathit{u}_\infty`$

também temos que todo universo pertence a um universo superior, eis a notação geral: $`\mathit{u}_n \in \mathit{u}_{n+1}`$ ou para ser mais geral: $`\in_{n=1}^{\infty} u_n`$

i.e, em Agda, Set está em Set_1 que está em Set_2 que está em Set_3... uma hierarquia de universos infinitos cumulativos ascendentes

o livro responde a uma pergunta interessante: isso não leva ao paradoxo de Russell? não, porque há uma hierarquia de universos

no caso, pode também ser representado por $`\mathit{u}_n : \mathit{u}_{n+1}`$

um pequeno alerta

[imagem da página 24 concernente a anotação ambígua de tipos/universos]

existe funções como $`B : A \to \mathit{u}`$ que se chamam família de tipos ou tipos dependentes, porque eles são bem gerais (já adiantando o tópico sobre tipos dependentes, porque tipos dependentes são tão poderosos que nos fazem refletir se o nosso conceito de generalização é mesmo tão geral)

o book também dá outro exemplo de família de tipos: $`Fin : \mathbb{N} \to \mathit{u}`$, cujo número de elementos tem a mesma quantidade que o argumento
