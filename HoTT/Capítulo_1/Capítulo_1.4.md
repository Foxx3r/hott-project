aqui estão algumas informações sobre tipos dependentes e sintaxe

[imagem da página 25]

basicamente, o que ele está querendo dizer com B ser constante é que $`B : \mathit{u}`$

i.e, $`\prod_{(x:A)} B`$ mapeia A para B, i.e, A para u. Também podemos interpretar como $`\forall{(x:A)}.B`$

para a função $`f : \prod_{(x:A)} B(x)`$, com $`B : A \to \mathit{u}`$, então temos na realidade $`f : A \to \mathit{u}`$, ou $`f : \forall{(x:A)}.B(x)`$, ou seja, se A for um número natural, o codomínio dependerá de A

um exemplo que gosto muito é da Wikipedia: $`\prod_{(n:\mathbb{N})} Vec(\mathbb{R}, n)`$, onde se retorna um tipo do mesmo tamanho de n, por exemplo, $`Vec(2) : suc\ (suc\ zero`)$ ou $`Vec(0) : zero`$

i.e, podemos resumir tipos dependentes como funções que dependem de funções. Um exemplo que também amo é esse usando programação: $`div : \mathbb{R} \to \{x: \mathbb{R}\ |\ x \neq 0\} \to \mathbb{R}`$, onde o segundo elemento da divisão não pode ser 0

com isso, podemos até provar propriedades de tipos só usando tipos, p. ex, provar que uma certa função é associativa usando somente tipos

um exemplo perfeito dado no livro: a função polimorfica id. $`id : \prod_{A:\mathit{u}} A \to A`$, que é o mesmo que $`id : \forall{A}.A \to A`$ ou $`id : \mathit{u} \to \mathit{u}`$

outro exemplo perfeito (ignore esses A, B, C, eles só atrapalham na leitura)

[imagem da página 26]

onde g(a, b) vira g(b, a)

vou dar só uma conclusão do que tipos dependentes fazem:
- quantificação universal
- polimorfismo
- restringe argumentos (como no caso do div)
- prova automaticamente proposições de tipos
- cria funções onde o retorno é dependente do argumento anterior
- tipo que cria tipos (como no exemplo de Fin e Vec)

lembrando que variável livre (free) é a variável que não está ligada à função, nem como argumento nem como variável local, como $`\lambda{x}.g(x, y)`$, y é uma variável livre e x é uma variável ligada (bound) ao escopo

por isso que o teorema SMD diz que no macro, a função de demanda pode assumir qualquer forma, mesmo que estejamos falando de agentes racionais (homus economicus), mas enfim, o canal não é de economia

e mais uma coisa: vou dar 2 exemplos de tipos dependentes, um que chamamos de refinement type e outro de prova

$`div : \prod_{m, n : \mathbb{N}} GT(n, 0)`$

onde GT(n, 0) significa "n is greater than 0", porque não se pode dividir por 0. Então restringimos a função dependente, ela depende da função GT, porque se n = 0, como em 5 dividido por 0, ela não executa, ela tem o domínio restringido. Tais funções com o domínio restringido, chamamos de refinement type, que é definível não somente em linguagens dependentes (p ex, Liquid Haskell tem, Pascal também)

$`assoc : \prod_{m, n, p : \mathbb{N}} \to (m + n) + p = m + (n + p)\newline`$
$`assoc :\equiv refl`$

aqui basicamente provamos que a operação de adição é associativa (i.e, tanto faz a ordem dos parênteses), um exemplo simples mas útil para entendermos. Não se preocupe com o refl, só coloquei ele para você já se acostumar com ele, mas não precisa entender ele. Basicamente ele é o único valor possível para esse tipo de prova, pois o único valor de = é refl, então sempre que usarmos =, usaremos esse refl. Se a prova acima não estiver correta, ela falhará (isso em uma linguagem de programação, mas você pode tentar derivar e provar a sua proposição assim como em qualquer teoria matemática)

novamente, isso significa $`assoc : \forall{(m, n, p : \mathbb{N})} \to (m + n) + p = m + (n + p)`$, ou em português, "para todo m, n e p nos números naturais, então (m + n) + p = m + (n + p)"
