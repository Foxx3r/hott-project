aqui descobrimos que há um tipo 1 cujo único valor é *, isso seria equivalente a
```hs
data One = Star
```

em Haskell

[imagem da página 26]

aqui se fala como criar tipos

[imagem da página 27]

bem, como dizia, (i) introduz o conceito de regras de formação (tipos são exatamente regras de formação), após a regra de formação, geralmente temos o (ii) que é a regra de introdução, após isso temos (iii) as regras de eliminação que especifica como devemos eliminar (avaliar a função) e (iv) aplica a própria regra de eliminação (regra computacional = redução-beta) e (v) introduz regras de igualdade

esse capítulo também introduz a noção de projeção, que vem de teoria dos conjuntos:

$\newline proj_1 : \prod_{(A, B : \mathit{u})} A \to B \to A\newline`$
$`proj_1 ((a, b)) :\equiv a\newline\newline`$

$`proj_2 : \prod_{(A, B : \mathit{u})} A \to B \to B\newline`$
$`proj_2 ((a, b)) :\equiv b`$

também recomendo entender a função rec que aparece como exemplo no livro

o livro também dá um exemplo muito interessante

$`A : \mathit{u}\\`$
$`B : \mathit{u}\\`$
$`uppt : \prod_{x : A \times B} ((proj_1(x), proj_2(x)) = x)\\`$
$`uppt((a, b)) = refl`$

observação interessante: uma recursão é chamada de eliminadora não-dependente e uma indução de eliminação dependente

uma propriedade importante da indução é como aparece em um próprio exemplo dado no livro

$`upun : \prod_{x : 1} x = *\\`$
$`upun :\equiv refl`$
