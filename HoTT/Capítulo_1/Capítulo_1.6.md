não há nada de muito diferente do que já vimos com produto dependente

[imagem da página 30]

se $`B : \mathit{u}`$ e $`\sum_{(x:A)} B \equiv (A \times B)`$ e se $`B : A \to \mathit{u}`$ e $`\sum_{(x:A)} B(x) \equiv A \times \mathit{u}`$

já vou adiantando que isso é equivalente a sentença lógica quantificacional $`\exists{(x:A}).B`$, ou literalmente, "há algum x em A tal que A e B"

ah, uma questão interessante: isso quer dizer que ambos são iguais? em tese sim

mas a diferença é que o B(x) depende de x, enquanto o primeiro apenas quantifica a variável

só um adendo: ao invés de $`\prod`$, o $`\sum`$ engloba todo o escopo, por isso você vê um monte de $`\prod`$ instanciados, vários depois de seus respectivos morfismos

exemplo:

$`proj_1 : (\sum_{(x:A)} B(x)) \to A\\
proj_1 ((a, b)) :\equiv a`$

que ficaria o mesmo que $`proj_1 : (A \times A) \to A`$, pois B(x) depende de x

depois, há vários exemplos

eles já dão um exemplo bem avançado: o que é magma
$`\\Magma :\equiv \sum_{(A:\mathit{u})} (A \to A \to A)`$

que é basicamente uma operação binária, como você pode ver

magmas são apenas grupos fracos com funções binárias https://en.m.wikipedia.org/wiki/Magma_(algebra)
