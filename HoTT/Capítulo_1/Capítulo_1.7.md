a noção de coproduto é caracterizada por $`A + B`$ onde $`A, B : \mathit{u}`$. $`A + B`$ é chamado de união disjunta

também há o tipo vazio $`0 : \mathit{u}`$, onde há duas maneiras de construir $`foo(a) : A + B`$ para $a : A$ ou $a : B$, mas não há nenhuma maneira de construir tipos vazios
