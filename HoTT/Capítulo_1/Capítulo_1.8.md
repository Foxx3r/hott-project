há o tipo dos booleanos (é horrível, mas finja que não é) $`2 : \mathit{u}`$, cujos elementos são $`0_2, 1_2 : 2`$ (note que é preciso do underline para contextualizar), $`0_2`$ para falso e $`1_2`$ para verdadeiro

então até agora tivemos o tipo da unidade $`1_* : *`$, o tipo vazio $`0 : \mathit{u}`$ (sem nenhum elemento, é apenas o tipo vazio, que ao contrário da unidade e dos booleanos, não tem construtores/elementos) e o tipo dos booleanos $`0_2, 1_2 : 2`$
