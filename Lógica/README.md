**1. Operadores verofuncionais (conectivos**

Em lógica proposicional, temos os chamados operadores verofuncionais (que retornam verdadeiro ou falso, i.e, verifica a verdade de uma proposição) ou conectivos lógicos. Vou listar alguns:

**Implicação**

Quando dizemos "A implica B", escrevemos isso em lógica como $`A \implies B`$, ou $`A \to B`$ (há uma distinção entre os 2 símbolos, mas é tão confusa que sequer os autores distinguem).  Por exemplo, se temos A = ficar na chuva e B = pegar resfriado, então se A é verdadeiro, logo B.

**Implicação material**

Quando dizemos "A se e somente se B", representamos isso por $`A \Leftrightarrow B`$, que significa que se os 2 forem iguais (os 2 são falsos ou verdadeiros), então a sentença/proposição é verdadeira. Algumas vezes você vai ver dizendo em português "A sse B" ou em inglês "A iff B"

**Negação**

A negação de A é $`\neg{A}`$, ou seja, se A é verdadeiro, sua negação é falso. Se A é falso, sua negação é verdadeiro. As vezes a negação de A também é representada por $`\sim{A}`$

**Conjunção**

Quando dizemos "A e B", essa proposição é representada conectivamente por $`A \land B`$, isso significa que se A e B são verdadeiros, então a sentença é verdadeira. Também é representado por $`A\ \&\ B`$

**Disjunção**

Quando dizemos que A ou B, isso é representado por $`A \lor B`$, se ou A ou B é verdadeiro, então a sentença é verdadeira. Também é chamado de disjunção inclusiva. Também é representado por A+B

O terceiro excluído (LEM) é a lei lógica que diz que só há verdadeiro e falso. As correntes construtivistas (incluindo a intuicionista) negam o terceiro excluído, sustentando que há um terceiro valor, que não é nem A nem não-A. Vemos este resultado, por exemplo, no teorema da incompletude de Gödel, onde uma prova não é somente verdadeira ou falsa, mas também indecidível. Podemos ver, então, que o LEM é onisciente, seria afirmar que tudo é verdadeiro ou falso; e ainda introduz uma versão mecanicista de lógica, que é externa ao próprio objeto em questão (daí o porque lógicas dialéticas se põem como imanentes enquanto a clássica se põe como lógica do discurso, somente), como se o mundo não mudasse de forma qualitativa, como se sempre fosse verdadeiro ou falso, não se enxergando a unidade do verdadeiro e do falso (como oposição determinada, isto é, específica daquele objeto, e não mera oposição intensional, onde tudo é a negação do outro, por serem qualitativamente diferentes), nem como se passa dum para o outro, como se contraexemplos achados em provas não a ajudassem a atingir graus de verdade superiores, um mundo sem mudança, sem quebra de simetria, que só um oráculo onisciente seria capaz de resolver. Viu como ao negar o terceiro excluído, internalizamos questões que a matemática clássica trata como metamatemáticas, filosóficas e epistemológicas?

Simplesmente porque a lógica clássica é somente a lógica do discurso (e focada muito mais na sintaxe, algo que não pode ser resolvido mesmo com interpretações semânticas, porque o ponto de vista sintático é próprio da lógica formalista): ela vê a realidade por um ponto de vista bem local, mas é necessário olhar do "ponto de vista" da totalidade para ter noção de toda a verdade sobre o objeto em questão (assim como uma "triangulogia" perderia verdades importantes sobre triângulos, pois o estudo de triângulos está também relacionado com outras figuras, como a circunscrição de triângulos em círculos), só que essa visão é ainda puramente formal baseada em axiomas e regras de inferência, mas não irei me alongar quanto a este tema. O último tema é um espantalho frequentemente usado contra construtivistas: negar o LEM é negar 99% da matemática. Não, porque 1) Robert Harper argumenta que pela matemática construtivista ter uma lógica mais fraca, é bem mais poderosa que a clássica; 2) você pode representar 99% das proposições clássicas usando a translação Gödel-Gentzen; 3) só não podem ser traduzidas proposições que afirmam que o terceiro excluído vale universalmente e 4) é um fato bem conhecido que a maior parte da matemática é informal.

A lei do terceiro excluído é caracterizada por $`A \land \not{A}`$, que pode ser desmentida pela primeira versão do cálculo de sequentes (que a princípio não necessitava do terceiro excluído e que tal proposição inclusive era refutada pela eliminação do corte, mas que Gentzen adicionou o LEM como axioma pois sua teoria não seria bem aceita na época), que é uma espécie de raciocínio lógico que segue a forma mais natural de como raciocinamos

e basicamente o princípio da explosão diz que tudo se segue a partir de uma contradição, algo falso. É o que queremos dizer com "o sistema de tipos explode", por exemplo, quando lidamos com teoria dos tipos

por último, temos a lei de Peirce, que afirma que $`((P \to Q) \to P) \to P`$, e note que isso implica na universalidade do terceiro excluído, então não é algo tomado como verdade nas lógicas construtivistas

**3. NOR, NAND e XOR**

Essas operações são estudadas mais comumente em processamento digital de sinais e afins, mas tem certo valor lógico. O NOR equivaleria, em linguagem natural, a "nem ... nem", sendo representado por uma seta para baixo $`P \downarrow Q`$, ou "nem P nem Q", cuja proposição só é verdadeira se P e Q forem falsos.

Também temos o NAND, o "não-ambos", que só é falso caso P e Q forem verdadeiros. É representado por uma seta para cima, como $`P \uparrow Q`$

O XOR é representado pelo símbolo $`\oplus`$e difere do "ou" mais comum por ser exclusivo. Por exemplo, quando dizemos "ele é Uber ou cientista da computação", essa sentença é verdadeira se ele for Uber e cientista da computação ao mesmo tempo, ou seja, ele deve ser apenas um dos 2 no "ou" exclusivo, enquanto no inclusivo, dizemos que a casa é amarela ou o Obama é negro, esta é uma sentença válida.

**4. Inverso, converso e contra-positivo**

O inverso de $`P \to Q`$ é $`\neg{P} \to \neg{Q}`$. O converso de $`P \to Q`$ é $`Q \to P`$. O contra-positivo de $`P \to Q`$ é $`\neg{Q} \to \neg{P}`$.

**5. Tabela-verdade**

Tabela-verdade é um método para dispor em uma tabela o resultado lógico dos conectivos. Seu tamanho é 2ⁿ onde n é o número de átomos da proposição. Mais adiante, veremos métodos melhores para visualizar essas verdades lógicas (e que inclusive são mais computacionalmente tratáveis), como tableuax semânticos e dedução natural. Um exemplo de tabela-verdade:

[imagem de tabela verdade]

**6. Tautologia, contradição e contingência**

Uma tautologia é uma proposição lógica que é sempre verdadeira na tabela-verdade. Uma contradição é sempre falsa. Uma contingência varia entre verdadeiro e falso. Um exemplo de contradição:

**7. Conclusão e porque**

O símbolo $`\therefore`$ significa portanto, e logo veremos ele em ação. Já o porque não é tão usado, mas é válido colocá-lo aqui. Seu símbolo é $`\because`$

**8. Regras de inferência**

Regras de inferência são regras sintáticas para derivar conclusões sobre a proposição. Vamos ver algumas das regras de inferência mais comuns no nosso dia-a-dia da lógica:

**Adição**

Se temos algum A ou B, podemos introduzir a disjunção deles, ou simplesmente, $`\\\frac{A\\B}{\therefore{A \lor B}}`$

**Conjunção**

Se temos dois átomos, podemos fazer a conjunção deles. Por exemplo, $`\\\frac{A\\B}{\therefore{A \land B}}`$

**Simplificação**

Se temos uma conjunção de A e B, podemos eliminar um deles, simplificar. Portanto, $`\\\frac{A \land B}{\therefore{A}}`$

**Modus ponens**

Modus ponens é também conhecido como eliminação condicional, porque ele elimina o antecedente simplificando, ficando apenas com o sucedente. Portanto, $`\\\frac{A \to B, A}{\therefore{B}}`$

**Modus tollens**

Leva a uma conclusão acerca da tabela-verdade da condicional. Portanto, $`\\\frac{A \to B, \neg{B}}{\therefore{\neg{A}}}`$

**Silogismo disjuntivo**

O silogismo faz a simplificação da disjunção. Portanto, $`\\\frac{A \lor B, \neg{B}}{\therefore{A}}`$

**Silogismo hipotético**

Este silogismo evidencia a relação transitiva da condicional. Portanto, $`\\\frac{A \to B, B \to C}{\therefore{A \to C}}`$

**Dilema construtivo**

Este evidencia que se temos duas condicionais e temos uma disjunção entre seus antecedentes, podemos simplificar para uma disjunção de seus sucedentes. Portanto, $`\\\frac{(A \to B), (C \to D), A \lor C}{\therefore{B \lor D}}`$

**Dilema destrutivo**

Este eu acho auto-evidente: $`\\\frac{(A \to B) \land (C \to D), \neg{B} \lor \neg{D}}{\therefore{\neg{A} \lor \neg{C}}}`$

**9. Consequência sintática e semântica**

Quando dizemos "A prova B", representamos isso logicamente por $`A \vdash B`$, e quando queremos dizer "A está provado", caracterizamos por $`\vdash A`$. Isso se chama consequência sintática. Há também a consequência semântica, i.e, $`A \vDash B`$, que é uma coisa que provavelmente não estudaremos (realizabilidade e teoria de modelos) mas pode aparecer no futuro.

**10. Tableuax senânticos**

Tableaux semântico é um método para se provar proposições lógicas sem usar tabela-verdade (e ele se difere entre outros aspectos que a tabela-verdade não consegue realizar). As premissas são escritas numa horizontal e suas alternativas de interpretação são derivadas na diagonal, assim construindo uma prova parecida com uma árvore.

[imagem de um exemplo]

**11. Q.E.D**

Quando se termina uma prova, se coloca $`\blacksquare`$ no final. Significa Quod Erat Demonstratum, ou "como se queria demonstrar".

**12. Domínio de discurso**

Também chamado de universo de discurso, toda proposição tem (e deve ter) um universo de discurso. Ele contextualiza a proposição. Por exemplo, ao dizemos "todo número elevado a quadrado é diferente de 2" é ambíguo porque não sabemos se essa proposição é sobre os números reais ou naturais. Portanto, anotamos: $`\mathbb{D}:\mathbb{N}`$

**13. Lógica de predicados**

A lógica de predicados consegue estabelecer relações e determinar qualidades de objetos, por exemplo $`pai(x, y)`$ diz que x é pai de y. $`homem(socrates)`$ diz que socrates é homem.

**14. Quantificação**

Podemos definir que uma proposição vale para todo x, com $`\forall{x}.P(x)`$, e podemos dizer que há pelo menos algum x que vale a proposição, com $`\exists{x}.P(x)`$

**Cálculo de sequentes**

Qual a necessidade do cálculo de sequentes? Simplesmente é uma maneira bem mais natural de interpretar a lógica, e inclusive é a melhor maneira para computadores trabalharem. Mas o que é um sequente? É simplesmente o símbolo $`\vdash`$, que também pode ser representado por $`\to`$. Então um sequente tem a forma geral $`\Gamma \vdash \Delta`$, onde $`\Gamma`$ e $`\Delta`$ são conjuntos de proposições.

Também temos a forma geral $`p_1 \land p_2 \land p_3 \land ... \vdash P_1 \lor P_2 \lor P_3 \lor ...`$, ou seja, tudo à esquerda prova alguma coisa à direita

Aqui temos a regra de inferência chamada introdução (também chamada de regra de introdução), que é um axioma, dizendo que a partir de A, podemos provar A. Note que em cima, está vazio, isso porque a conclusão é derivada "do nada" (axioma). Note que também sempre que derivamos alguma proposição, temos que colocar ao lado entre parênteses qual regra a gente usou para derivar a proposição. Neste caso, estamos definindo a regra de introdução; não necessariamente usando ela em alguma proposição.

[imagem]

Aqui temos uma das regras mais importantes de todo o cálculo de sequentes: a regra do corte (Cut). Suponhamos que gamma = Bohm, delta = Einstein, A = Bohr, zeta = Heisenberg, pi = Born, então na primeira linha temos a proposição "Bohm prova que ou Einstein ou Bohr estava certo E Bohr e Heisenberg provam que Born estava certo", então pela regra do corte, podemos derivar que "Bohm e Heisenberg provam que ou Einstein ou Born estavam certos"

[imagem]

há também o teorema da eliminação do corte (Hauptsatz), o resultado mais importante de Gentzen, onde ele mostra que uma prova com corte e sem corte são a mesma coisa

aqui há mais inúmeras regras de inferência, onde L = esquerda, R = direita, $`(\to{R})`$ significa "sucedente a direita" e assim por diante

[imagem]

e aqui temos as regras estruturais, onde WL = weakening a esquerda, onde a regra de weakening diz que adicionar alguma premissa não altera o resultado, e a regra de contração a esquerda (CL) diz que retirar uma premissa não altera o resultado, e P = permutação

[imagem]

ê por causa dessas regras estruturais que temos as lógicas subestruturais (as mais conhecidas são lógica linear e lógica afim), onde algumas regras estruturais não se aplicam a elas, por exemplo, como a lógica linear é uma lógica de recursos, portanto, adicionar ou tirar algo da proposição altera sim o resultado, então a regra de weakening e contração não se aplica

e para finalizar:
1. Há dois sistemas de cálculo de sequentes, o cálculo LK (clássico) e o cálculo LJ (intuicionista)
2. O criador do cálculo de sequentes é o Gentzen, pai da teoria da prova, refinando o sistema de Hilbert de dedução, criado em 1931
3. Gentzen primeiro criou seu cálculo de sequentes seguindo a maneira natural de como a mente deduz proposições, e acabou chegando a um sistema onde o terceiro excluído não valia, e como isso era mal visto na época, ele adicionou o terceiro excluído, então a única diferença entre o sistema LK e o LJ é que no LK há um axioma a mais: o terceiro excluído

**Dedução natural**

Existem dois sistemas de dedução natural: NK (clássico) e NJ (intuicionista). O NK não é simétrico. Existem poucas diferenças entre dedução natural e sequentes, mas um pode ser traduzido para o outro. As principais diferenças é que a regra do corte é implícita na dedução natural (já está contida dentro da regra de introdução e eliminação de sucedente) e você só prova uma coisa, não uma disjunção

esqueci de dizer que a regra de eliminação (E) é uma regra de como utilizar recursos, por exemplo, a redução-beta do lambda cálculo é uma regra de eliminação
