**PLFA: capítulo 1, naturais**

É introduzida a noção de **caso base**, que será a base da indução (por exemplo, a definição de 0) e o **caso indutivo**, onde será construída a indução (por exemplo, S(n + 1)). O caso base não tem nenhuma hipótese (pois não precisa assumir nada para definir 0), e o caso indutivo tem uma hipótese (que assume que S(n + 1) também é um natural).

definição de adição

```
_+_ : ℕ → ℕ → ℕ
zero + n = n
(suc m) + n = suc (m + n)
```

vamos ver o que acontece se por exemplo, nós fizermos 2 + 3

```
(suc 1) + (suc 2)
(suc (suc zero)) + (suc (suc (suc zero))) = suc (suc zero + (suc (suc (suc zero)))
suc (suc zero + (suc (suc (suc zero))) = suc (suc (zero + suc (suc (suc zero))))
suc (suc (zero + suc (suc (suc zero)))) = suc (suc (zero + 3))
suc (suc 3)
suc 4
5
```

hmm, eles deram um exemplo até melhor que o meu

[imagem da prova de que 2 + 3 = 5]
