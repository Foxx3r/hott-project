não vai ser possível continuar pro capítulo 2 de HoTT enquanto você não souber álgebra abstrata, então veja essa série de vídeos, vai ser o suficiente (talvez) https://www.youtube.com/playlist?list=PLi01XoE8jYoi3SgnnGorR_XOW3IcK-TP6

**1. O que é álgebra abstrata?**

Álgebra abstrata é também chamada de álgebra moderna e estuda estruturas matemáticas algébricas. Foi criada em 1800 por Evariste Galois, com teoria dos grupos, se perguntando se haveria um método geral de resolver equações quínticas por radicais.

**2. Definição de grupo**

No vídeo, é dado o exemplo de um relógio de 6 horas, com operações aritméticas definidas e um conjunto {0, 1, 2, 3, 4, 5, 6} de horas. 3 + 5 = 1, por exemplo. Essa aritmética é chamada de modular, então o grupo que estamos trabalhando é $`(G, \mathbb{Z} mod 7)`$, onde G é o nome do nosso grupo do relógio de 6 horas.

também é dado um exemplo de rotação de triângulos, onde 1 = mesmo triângulo, f = vira, r = rotação

r = uma rotação de 120 graus<br/>
r² = rotação de 240 graus<br/>
r³ = rotação de 360 graus = 1<br/>
f = vira de lado<br/>
f² = vira de lado duas vezes = 1<br/>
r * f = roda e vira

essas operações que transformam a forma de volta a si mesmo é chamado de simetrias, e novamente, aqui, temos um exemplo de aritmética modular

o último exemplo é sobre o conjunto dos números inteiros $`\mathbb{Z}`$, onde temos apenas uma única operação, que é +, porque - é definido em termos de adição de negativos, por exemplo 3 - 11 = 3 + (-11), então temos o grupo $`(\mathbb{Z}, +)`$, e como a adição de dois inteiros resulta em outro inteiro, dizemos que o grupo é "fechado sob a adição", um contraexemplo seria a operação de divisão, onde $`\frac{2}{3}`$ não está nos inteiros, e temos também um elemento identidade, que não muda o resultado da operação de adição: 0, porque x + 0 = x
a
portanto, um grupo tem as seguintes propriedades:
- conjunto de elementos (no nosso exemplo, foi G)
- operação: +, *, -
- fechado sob a operação (para todo x e y em G, x * y está em G)
- inverso (x^-1)
- identidade
- associatividade

em todos esses exemplos, percebemos que os grupos estão por toda a parte, e é este o objetivo da álgebra abstrata (teoria das categorias é uma área da álgebra abstrata que estuda categorias, outro nível de abstração de estruturas matemáticas)

**5. O que é um subgrupo?**

Um subgrupo é um subconjunto de um grupo G, denotado por H. Dizemos que $`G \leq G`$, ou seja, G é um subgrupo de si mesmo. Quando o subgrupo não é igual ao seu grupo, dizemos que $`H < G`$, ou H é um subgrupo próprio de G. Um exemplo seria um grupo $`(G, +)`$, onde G = \{0, 1\}, que é um subgrupo do grupo dos inteiros $`\mathbb{Z}`$

**6. Tabela de Cayley**

É uma tabela que mostra resultados da multiplicação no grupo. O vídeo mostra algumas propriedades interessantes da tabela de Cayley, que também ajuda a identificar o tipo do grupo e prever o resultado de tabelas incompletas. Também é apresentado o conceito de **ordem**, que significa número de elementos, denotado por |G|, então um grupo de ordem 1 (1 elemento apenas, o de identidade) é chamado de **grupo trivial**.

**7. Teorema de Lagrange**

O teorema de Lagrange apenas diz que $`H \leq G = |H| \div |G|`$, e isso tem uma implicação muito legal: se pegarmos um grupo G com 323 elementos, então seus divisores são 1, 17, 19 e 323. Isso significa que temos apenas 4 possíveis subgrupos do grupo G, que tem 1 elemento (grupo trivial), 17 elementos, 19 elementos e 323 elementos (ele próprio). Então só a partir desses subgrupos que é possível definir um subconjunto que garanta propriedades que façam com que ele continue sendo um grupo.

um remark muito interessante dela: o teorema de Lagrange não diz que existam subgrupos com 1, 2, 3, 6, 12 elementos... mas que possivelmente existe. Por exemplo, pode existir subgrupos de ordem 1, 2, 3 e 12 mas pode não existir de ordem 6, isso acontece por causa da operação

também é apresentado um conceito muito importante chamado coclasse (coset em inglês), que é uma modificação de um grupo (que nem sempre forma um grupo), então se temos um grupo H, onde H = \{1, 2, 3, 4\}, então podemos definir um coset à esquerda $`xH = \{xh : h \in H\}`$, ou um coset à direita $`Hx = \{hx : h \in H\}`$, onde 2H = \{2, 4, 6, 8\}, lembrando que cosets só se aplicam a grupos não-comutativos (i.e, grupos não-abelianos)

2H é um coset

**8. Subgrupo normal e grupo quociente (ou factor group)**

É introduzido a noção de **classes de equivalência**, se pegarmos o coset de $`\mathbb{Z} mod 5`$, então temos 5 classes de equivalência da aritmética modular nos inteiros:
$`\\0 = \{..., -15, -10, -5, 0, ...\}\\
1 = \{..., -14, -9, -4, 1, ...\}\\
2 = \{..., -13, -8, -3, 2, ...\}\\
3 = \{..., -12, -7, -2, 3, ...\}\\
4 = \{..., -11, -6, -1, 4, ...\}`$

note que não temos 5 pois 5 = 0, e uma importante propriedade é que se somarmos algum elemento do conjunto 1 com o conjunto 2, você terá um elemento do conjunto 3, e se você somar algum elemento do conjunto 4 com algum elemento do conjunto 2, teremos um elemento do conjunto 1

como os cosets de $`5\mathbb{Z}`$ formam um subgrupo, então chamamos ele de **subgrupo normal**, e o grupo dos cosets é chamado de **grupo quociente**

fun fact: é impossível resolver equações de quinto grau por um método geral de radicais porque quando a equação se torna de quinto grau, então ela deixa de ser um subgrupo normal

em álgebra abstrata, não escrevemos $`\mathbb{Z} mod 5`$, mas $`\mathbb{Z}/5\mathbb{Z}`$

**9. Grupo cíclico**

Um grupo é dito cíclico se todos os seus elementos podem ser gerados a partir de um único número, por exemplo, o conjunto dos números naturais $`\mathbb{N}`$ pode ser gerado da adição de 1 a ele próprio, então o conjunto dos números naturais sob a adição é cíclico

**11. Homomorfismo de grupo**

Um homomorfismo de grupo é uma função $`f : G \to H`$, onde suponhamos que tenhamos dois grupos $`(G, \ast)`$ e $`(H, \cdot)`$, então $`f(x \ast y) = f(x) \cdot f(y)`$

i.e, f(x) converte um elemento de G em um elemento de H

**13. Isomorfismo**

Um homomorfismo é um isomorfismo se para elemento de G, há um equivalente em H, caso contrário, ele não é isomórfico

**14. Kernel de um homomorfismo de grupo**

O kernel é uma função de alta ordem, i.e, ele recebe uma função (ou homomorfismo, tanto faz como você chame em álgebra abstrata) e retorna todos os elementos de G que quando aplicados a função, retornam a identidade em H. Ele é definido por $`\\ker(f) = \{x \in G | f(x) = 1_H\}`$

**15. Grupos simétricos**

É introduzida a noção de função de permutação (que, note, é isomórfica), onde podemos recombinar elementos de conjuntos de uma outra forma. Por exemplo, $`\\f : \{1, 2, 3\} \to \{1, 2, 3\}\\f(1) = 2\\f(2) = 3\\f(3) = 1\\`$ transformando, assim, o conjunto \{1, 2, 3\} em \{2, 3, 1\}

quando temos um grupo com todas essas permutações, então temos o grupo simétrico $`S_n`$, onde podemos recombiná-lo de n! maneiras. Por exemplo, no exemplo acima, o grupo é $`S_3`$, i.e, podemos recombiná-lo de 9 maneiras diferentes.

importante: todo grupo finito é um subgrupo de algum grupo simétrico

não acho relevante para nosso propósito, mas é uma coisa legal https://youtu.be/MpKG6FmcIHk

**Algumas estruturas algébricas**

**Magma**: tem uma operação binária<br/>
**Semigrupo**: tem uma operação binária associativa<br/>
**Monoid**: tem uma operação binária associativa e um elemento identidade<br/>
**Grupo**: tem uma operação binária associativa, tem inverso e um elemento identidade<br/>
**Grupo abeliano**: tem uma operação binária associativa e comutativa, tem inverso e um elemento identidade<br/>
**Anel**: tem uma operação binária associativa e não-comutativa, tem inverso e um elemento identidade<br/>
**Corpo**: tem adição, subtração, multiplicação e divisão
